# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "0.4.2"
  end
end
